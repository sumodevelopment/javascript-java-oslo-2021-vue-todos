# Vue Todo
- Create a Todo App!
    - List Todos
    - Add a Todo
    - Complete a Todo
    - Search/Filter (Optional)
    - User page (Optional)
    
- Extra
    - Remote data using fetch()
    - Easy-setup using JSON-Server
    
## Routes
- Todos
- Single Todo /:todoId
- Profile (Optional)

## Features
- Todos
  - Todos (View)

- TodoDetail
  - TodoDetail (view)

- Profile
  - Profile (view)
