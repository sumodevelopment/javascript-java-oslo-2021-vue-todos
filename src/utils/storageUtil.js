export function setStorage(key, value) {
    try {
        const json = JSON.stringify(value)
        const encoded = btoa(json)
        localStorage.setItem(key, encoded)
    } catch (e) {
        console.log(e)
    }
}

export function getStorage(key) {
    const stored = localStorage.getItem(key)

    if (!stored) {
        return null
    }

    const decoded = atob(stored)
    return JSON.parse(decoded)
}
