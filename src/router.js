import VueRouter from 'vue-router'
import Login from './components/Login/Login'
import Todos from './components/Todos/Todos'
import TodoDetail from './components/TodoDetail/TodoDetail'
import Profile from './components/Profile/Profile'
import { store } from "@/store";

const protectedRoute = () => ({
    meta: {
        protect: true
    }
})

const routes = [
    {
        path: '/login',
        component: Login,
        alias: '/'
    },
    {
        path: '/todos',
        component: Todos,
        ...protectedRoute()
    },
    {
        path: '/todos/:todoId',
        component: TodoDetail,
        ...protectedRoute()
    },
    {
        path: '/profile',
        component: Profile,
        ...protectedRoute()
    }
]

const router = new VueRouter({ routes })

router.beforeEach((to, from, next) => {
    if (to.meta.protect && !store.state.user.username) {
        return next('/login')
    }
    next()
})

export default router
