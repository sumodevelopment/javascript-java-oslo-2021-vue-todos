export function fetchUser(userId) {
    return fetch(`http://localhost:3000/users/${ userId }`)
        .then(r => r.json())
}

export function updateUser(user) {
    return fetch(`http://localhost:3000/users/${ user.id }`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    })
        .then(r => r.json())
}
