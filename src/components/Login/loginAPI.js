export const STORAGE_KEY_USER = '_tdsh-ss' // Random string.
const AVATAR_BASE_URL = 'https://avatars.dicebear.com/4.5/api/male'

export const LoginAPI = {
    login(username) {
        return fetch(`http://localhost:3000/users?username=${ username }`)
            .then(r => r.json())
    },
    register(user) {
        return fetch('http://localhost:3000/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                ...BASE_USER,
                ...user,
                avatar: `${ AVATAR_BASE_URL }/${ user }.svg`
            })
        }).then(r => r.json())
    }
}

export function login(username) {
    return fetch(`http://localhost:3000/users?username=${ username }`)
        .then(r => r.json())
}

export function register(user) {
    return fetch('http://localhost:3000/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            ...BASE_USER,
            ...user,
            avatar: `${ AVATAR_BASE_URL }/${ user }.svg`
        })
    }).then(r => r.json())
}

export const BASE_USER = {
    username: '',
    avatar: '',
    email: '',
    tel: '',
    age: '',
    occupation: '',
    address: {
        line1: '',
        country: ''
    },
    active: 1
}
