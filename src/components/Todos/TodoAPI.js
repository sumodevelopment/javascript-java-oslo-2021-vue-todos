import { store } from "@/store";

export const TodoAPI = {
    fetchTodos() {

        const {id} = store.state.user

        return fetch(`http://localhost:3000/todos?active=1&userId=${ id }`)
            .then(response => response.json())
    },
    completeTodo(id) {
        return fetch(`http://localhost:3000/todos/${ id }`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({completed: true})
        }).then(r => r.json())
    },
    createTodo(title) {

        const {id} = store.state.user

        return fetch('http://localhost:3000/todos', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                completed: false,
                title,
                active: 1,
                userId: id
            })
        }).then(r => r.json())
    },
    restoreTodo(id) {
        return fetch(`http://localhost:3000/todos/${ id }`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({completed: false})
        }).then(r => r.json())
    },
    deleteTodo(id) {
        return fetch(`http://localhost:3000/todos/${ id }`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({active: 0})
        })
            .then(r => r.json())
    }
}
