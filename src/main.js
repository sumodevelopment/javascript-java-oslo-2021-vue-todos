import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router"
import router from './router'
import './main.css'
import { store } from "@/store";
import { getStorage } from "@/utils/storageUtil";
import { BASE_USER, STORAGE_KEY_USER } from "@/components/Login/loginAPI";

Vue.use( VueRouter )

Vue.config.productionTip = false

// Initialize Global state before the Router or App starts.
store.setUser(getStorage(STORAGE_KEY_USER) || BASE_USER)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
