import { BASE_USER, STORAGE_KEY_USER } from "@/components/Login/loginAPI";
import { setStorage } from "@/utils/storageUtil";
import { TodoAPI } from "@/components/Todos/TodoAPI";

export const store = {
    state: {
        user: {...BASE_USER},
        todos: []
    },

    // Todo Actions
    getTodos() {
        return TodoAPI.fetchTodos().then(todos => {
            this.setTodos(todos)
        })
    },
    setTodos(todos = []) {
        this.state.todos = todos
    },
    addTodo(title) {
        return TodoAPI.createTodo(title).then(newTodo => {
            this.state.todos = [ ...this.state.todos, newTodo ]
        })
    },
    restoreTodo(todoId) {
        // eslint-disable-next-line no-unused-vars
        return TodoAPI.restoreTodo(todoId).then(_ => {
            this.state.todos = this.state.todos.map(todo => {
                if (todo.id === todoId) {
                    return {
                        ...todo,
                        completed: false
                    }
                }
                return todo
            })
        })
    },
    deleteTodo(todoId) {
        // eslint-disable-next-line no-unused-vars
        return TodoAPI.deleteTodo(todoId).then(_ => {
            this.state.todos = this.state.todos.filter(todo => {
                return todo.id !== todoId
            })
        })
    },
    completeTodo(todoId) {
        // eslint-disable-next-line no-unused-vars
        return TodoAPI.completeTodo(todoId).then(_ => {
            this.state.todos = this.state.todos.map(todo => {
                if (todo.id === todoId) {
                    return {
                        ...todo,
                        completed: true
                    }
                }
                return todo
            })
        })
    },

    // User Actions
    setUser(user) {
        setStorage(STORAGE_KEY_USER, user)
        this.state.user = user
    }
}
